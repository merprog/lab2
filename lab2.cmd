@echo off
setlocal enabledelayedexpansion
chcp 1251

:: Опції з командного рядка
set "LogFile=%1"
set "BasePath=%2"
set "ProcessToKill=%3"
set "ArchivePath=%4"
set "RemoteComputerIP=%5"
set "LogSizeLimit=%6"
set "7ZipPath=C:\Program Files\7-Zip\7z.exe"


:: Перевірка наявності та створення log файлу
if not exist %LogFile% (
    echo %date% %time% > %LogFile%
    echo "File with name %LogFile% was opened or created" >> %LogFile%
)

:: Отримання часу з NTP серверу та оновлення системного часу
w32tm /config /manualpeerlist:"pool.ntp.org",0x8 /syncfromflags:manual /reliable:YES /update
w32tm /resync
echo %date% %time% > %LogFile%

:: Виведення списку запущених процесів
tasklist >> %LogFile%

:: Завершення процесу за ім'ям ProcessToKill
taskkill /IM "%ProcessToKill%" /F

:: Видалення файлів з певного шляху за вказаними умовами
set "DeletedFiles=0"
for %%A in (!BasePath!\*.tmp !BasePath!\temp*) do (
    del "%%A"
    if !errorlevel! equ 0 (
        echo Deleted file: %%A >> !LogFile!
        set /a "DeletedFiles+=1"
    )
)

:: Вивести кількість видалених файлів
echo Deleted !DeletedFiles! files. >> !LogFile!

:: Створення архіву .zip
set "CurrentDateTime=%date% %time%"
:: Замінюємо неприпустимі символи на підкреслення
set "CurrentDateTime=!CurrentDateTime:,=!"
set "CurrentDateTime=!CurrentDateTime::=_!"
set "ArchiveName=%CurrentDateTime%.zip"
"C:\Program Files\7-Zip\7z.exe" a -tzip "%ArchivePath%\%ArchiveName%" "%BasePath%\*"

:: Перевірка наявності архіву за попередній день
ForFiles /p "%ArchivePath%" /s /d -1 >nul
if %errorlevel% equ 0 (
    echo Archive for yesterday exist. >> %LogFile%
) else (
    echo Archive for yesterday not exist. >> %LogFile%
)
:: Видалення архівів, старших 30 днів
ForFiles /p "%ArchivePath%" /s /d -30 /c "cmd /c echo outdated file @file was deleted >> %LogFile%"
ForFiles /p "%ArchivePath%" /s /d -30 /c "cmd /c del @file"

:: Перевірка доступності Інтернету
ping google.com -n 1 >nul
if %errorlevel% equ 0 (
    echo Internet is accessible. >> %LogFile%
) else (
    echo Internet is not accessible. >> %LogFile%
)

:: Перевірка наявності комп'ютера за IP-адресою
ping %RemoteComputerIP% -n 1 > nul
if errorlevel 1 (
    echo "Computer with IP %RemoteComputerIP% is not reachable." >> %LogFile%
) else (
    echo "Computer with IP %RemoteComputerIP% is reachable." >> %LogFile%
    :: Завершення роботи комп'ютера
    shutdown /s /m \\%RemoteComputerIP% /f /t 0
    echo "Shutdown command sent to computer with IP %RemoteComputerIP%." >> %LogFile%
)

:: Отримання списку комп'ютерів в мережі
net view >> %LogFile%

:: Перевірка наявності комп'ютерів з файлу ipa.txt
for /F %%A in (ipa.txt) do (
    ping %%A -n 1 >nul
    if !errorlevel! equ 0 (
        echo Computer %%A is in a network. >> %LogFile%
    ) else (
        echo Computer %%A isn't in a network. >> %LogFile%
    )
)

:: Перевірка розміру log файлу
for %%F in ("%LogFile%") do (
    if %%~zF gtr %LogSizeLimit% (
        echo Log size is larger than %LogSizeLimit%B >> %LogFile%
    )
)

:: Отримання інформації про диски в системі
for /f "tokens=*" %%A in ('wmic logicaldisk get caption^,freespace^,size /format:list ^| find "="') do (
    set "Line=%%A"
    echo !Line! >> %LogFile%
)

systeminfo > "systeminfo+%date:/=-%_%time::=-%.txt"

:: Завершення роботи
endlocal